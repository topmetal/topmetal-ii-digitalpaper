\documentclass[a4paper,11pt]{article}
\pdfoutput=1 % if your are submitting a pdflatex (i.e. if you have
             % images in pdf, png or jpg format)

\usepackage{jinstpub} % for details on the use of the package, please
                      % see the JINST-author-manual
 
\usepackage{xspace}   % added, for definition
\usepackage{caption}  % added, for minipage
\usepackage{lineno}   % added, for line numbering

% added definition, begin
%terms
\newcommand{\TM}{\emph{Topmetal}\xspace}
\newcommand{\TMIIm}{\mbox{\emph{Topmetal-II\raise0.5ex\hbox{-}}}\xspace}
\newcommand{\Vtp}{\ensuremath{V_\text{TP}}\xspace}
\newcommand{\Vs}{\ensuremath{V_\text{step}}\xspace}
\newcommand{\Cinj}{\ensuremath{C_\text{inj}}\xspace}
\newcommand{\CinjVal}{5.5~{fF}\xspace}
\newcommand{\Vth}{\ensuremath{V_{\text{th}}}\xspace}
\newcommand{\Vthg}{\ensuremath{V_{\text{th}g}}\xspace}
\newcommand{\Vthp}{\ensuremath{V_{\text{th}p}}\xspace}

% for math
\DeclareMathOperator*{\erf}{erf}

% misc command
\newcommand{\Mark}[1]{\textcolor{red}{#1}}
\newcommand{\sym}[1]{\texttt{#1}}
% added definition, end

\title{Characterization of the column-based priority logic readout of \TMIIm CMOS pixel direct charge sensor}

%% %simple case: 2 authors, same institution
%% \author{A. Uthor}
%% \author{and A. Nother Author}
%% \affiliation{Institution,\\Address, Country}

% more complex case: 4 authors, 3 institutions, 2 footnotes
\author[a]{M. An,}
\author[a,b]{W. Zhang,}
\author[a]{L. Xiao,}
\author[a]{C. Gao,}
\author[a]{C. Chen,}
\author[b]{M. Han,}
\author[a]{G. Huang,}
\author[a]{R. Ji,}
\author[c]{X. Li,}
\author[a]{J. Liu,}
\author[b,1]{Y. Mei,\note{Corresponding author.}}
\author[a]{H. Pei,}
\author[d]{Q. Sun,}
\author[a,1]{X. Sun,}
\author[a]{K. Wang,}
\author[a]{P. Yang,}
\author[a]{and W. Zhou}

% The "\note" macro will give a warning: "Ignoring empty anchor..."
% you can safely ignore it.

\affiliation[a]{PLAC, Key Laboratory of Quark and Lepton Physics (MOE), Central China Normal University,\\Wuhan, Hubei 430079, China}
\affiliation[b]{Nuclear Science Division, Lawrence Berkeley National Laboratory,\\Berkeley, California 94720, USA}
\affiliation[c]{Institute of High Energy Physics Chinese Academy of Sciences,\\Beijing, 100049, China}
\affiliation[d]{Department of Physics, Southern Methodist University,\\Dallas, TX 75205, USA}

% e-mail addresses: only for the forresponding author
\emailAdd{ymei@lbl.gov, xmsun@phy.ccnu.edu.cn}


\abstract{We present the detailed study of the digital readout of \TMIIm CMOS pixel direct charge sensor.  \TMIIm is an integrated sensor with an array of $72\times72$ pixels each capable of directly collecting external charge through exposed metal electrodes in the topmost metal layer.  In addition to the time-shared multiplexing readout of the analog output from Charge Sensitive Amplifiers in each pixel, hits are also generated through comparators in each pixel with individually adjustable thresholds.  The hits are read out via a column-based priority logic structure, retaining both hit location and time information.  The in-array column-based priority logic features with a full clock-less circuitry hence there is no continuously running clock distributed in the pixel and matrix logic.  These characteristics enable its use as the instrumentation for the detection and measurement of ionizing radiation.  We studied the detailed working behavior and performance of this readout, and demonstrated its functional validity and potential in imaging applications.}

% \keywords{Only keywords from JINST's keywords list please}
\keywords{Electronic detector readout concepts (solid-state); Front-end electronics for detector readout; VLSI circuits; Pixelated detectors and associated VLSI electronics}


% \arxivnumber{1234.56789} % only if you have one
\arxivnumber{1608.06399} 

% \collaboration{\includegraphics[height=17mm]{example-image}\\[6pt]
%   XXX collaboration}
% or
% \collaboration[c]{on behalf of XXX collaboration}

% if you write for a special issue this may be useful
% \proceeding{N$^{\text{th}}$ Workshop on X\\
 \proceeding{Topical Workshop on Electronics for Particle Physics 2016,\\
%  when\\
 September 26th to 30th, 2016\\
%  where}  
 Karlsruhe, Germany}


\begin{document}
% \setpagewiselinenumbers   % numbering by page
\linenumbers
\maketitle
\flushbottom  % to make the text fill the height of the page
\section{Introduction}
\label{sec:intro}

Highly pixelated sensors such as CMOS image sensors and Monolithic Active Pixels Sensors (MAPS), have been successfully deployed in various fields.  In many nuclear/particle physics applications, the traditional readout is orders of magnitude slower than what is required for signal/data acquisition in order to achieve the physics goals.  Therefore, many novel readout schemes, designed to provide faster access the information collected in the pixels, have been developed over the years.  These schemes often exploit the characteristics of signal distribution among pixels, such as the sparseness or clustering in space and time of pixel hits, and the similarity in amplitudes.  Notable examples include column-based readout \cite{a, b, c} and row-based compression \cite{d, e}.

We implemented a column-based priority logic readout in a prototype pixel sensor called \TMIIm\cite{f}, aimed at improving the latency between a pixel hit and the availability of data off the chip.  \TMIIm is implemented in a 0.35~$\mu$m CMOS process.  It features a $72\times72$ pixel array with 83.2~$\mu$m pixel pitch.  Pixels in \TMIIm are sensitive to external charges arrived at an exposed metal electrode in the topmost layer in each pixel.  Pixel hits are generated by pixel-local comparators with tunable thresholds.  We chose a scheme that is clock-less in the pixel array to minimize the potential interference from digital switching noise.  Digital activity in the array only happens when the sensor receives hits.  The in-array column-based priority logic (without clock) drives the address of the pixel that is hit to the edge of the array immediately upon a hit, which minimizes the latency.  A sequential logic (with clock) is employed to sense the hit location and time at the edge of the array and then ship such information off the sensor.  A detailed study of the analog characteristics of \TMIIm is reported in \cite{f}.  This
paper focuses on the details of operation and functional validity of the digital readout.

\section{Sensor structure and operation}\label{sec:so}

A \TMIIm sensor, as shown in figure~\ref{fig:TMIImOverall}, contains an array of $72\times72$ sensitive pixels occupying a $6\times6~{mm^2}$ area.  Each pixel has an exposed metal patch in the topmost layer that can directly collect charge.  The charge signal is amplified by a Charge Sensitive Amplifier (CSA) in each pixel (figure~\ref{fig:pixStr}).  Based on the inspection of the analogue readout channel, natural light can cause as large amplitude response as tens of {mV}, which confirms the light illumination also results in charge signal.  The amplified charge signal is accessible through two channels.  The analog voltage signal is read out through a ``rolling shutter'' style time-shared multiplexer controlled by the array scan unit.  A digital hit signal is generated by an in-pixel comparator with per-pixel adjustable threshold, then read out through the column-based priority logic.

\begin{figure}[htbp]
\centering % \begin{center}/\end{center} takes some additional vertical space
\includegraphics[width=.3\textwidth]{img/TopmetalII-Photo}
\quad
\includegraphics[width=.5\textwidth]{fig/TopmetalII-BlockDiagram}
% "\includegraphics" from the "graphicx" permits to crop (trim+clip)
% and rotate (angle) and image (and much more)
\caption{Photograph of a \TMIIm sensor (left) and its top-level block diagram (right).  The chip is $8\times9~{mm^2}$ (blue box) in size, in which a $6\times6~{mm^2}$ charge sensitive area (red box) is located in the center of the sensor.  Major functional units are shown in the top-level block diagram.  The sensor is powered by analog supply \sym{AVDD} and digital supply \sym{DVDD}, which are individually regulated at nominal voltage of 3.3~{V}.}
\label{fig:TMIImOverall}
\end{figure}

\subsection{Column-based priority logic readout}

The overall readout has two parts: an in-array clock-less logic and a sequential logic at the bottom edge of the array.  The clock-less logic consists of a Priority Logic (PL) in each pixel and an Address Bus (AB) in each column.  The sequential logic includes a Column Readout Unit (CRU) placed at the bottom edge of each column and a multiplexer (MUX) collecting the outputs of all CRUs.  CRUs monitor the address changes on the ABs.  CRUs and the MUX are synchronous to shared clock \sym{CLK} and reset \sym{RST} signals.  The maximum allowable frequency of the shared clock is 80~{MHz}.

\subsubsection{Pixel hits and Priority Logic (PL)}

A schematic view of the circuit in a single pixel is shown in the red dashed box in figure~\ref{fig:pixStr}.  The exposed \TM electrode is directly connected to the input of the CSA.  A ring electrode (\sym{Gring}), which is in the same topmost metal layer as the \TM, surrounds the \TM while being isolated from it.  The stray capacitance between the \sym{Gring} and the \TM, $\Cinj\approx\CinjVal$, is a natural test capacitor that allows pulses applied on \sym{Gring} to inject charge into the CSA.  The CSA with $C_f\approx5~{fF}$ converts the injected charge to voltage signal (\sym{CSA\_OUT}) and feeds it into the comparator.  The threshold voltage applied to the $i$th pixel comparator $\Vth{_i}$ is the sum of a globally set voltage \Vthg with a locally adjustable offset $\Vthp{_i}$.  The step size of all the 4-bit DACs is globally adjustable as well.  The pixel-local 4-bit DAC is intended for compensating the threshold dispersion of the comparator across the entire array.  The CSA and the comparator are constantly active.

\begin{figure}[htbp]
\centering % \begin{center}/\end{center} takes some additional vertical space
\includegraphics[width=.9\textwidth]{fig/TopmetalII-DigitalStruct}
% "\includegraphics" from the "graphicx" permits to crop (trim+clip)
% and rotate (angle) and image (and much more)
\caption{Digital readout pathway from a single pixel to the edge of array.  Structures at
    pixel, column and edge-of-array levels are indicated in the red, green and blue dashed boxes,
    respectively.}
\label{fig:pixStr}
\end{figure}

Upon an event of \sym{CSA\_OUT} surpassing the threshold \Vth, the comparator asserts
$\sym{Flag}=1$, which propagates to an AND gate \sym{G0} (figure~\ref{fig:pixStr}).  The other input of \sym{G0}, \sym{Mask}, is used for disabling pixels from responding digitally.  This feature is exploited during the digital readout tests and imaging demonstration.  The \sym{Mask} together with the 4-bits for DAC in each pixel are set by a pixel-local 5-bit SRAM.  Writes to SRAMs are synchronous to array scan.  When \sym{G0} outputs 1, a hit is generated ($\sym{Hit}=1$) and the PL module is notified.  Each PL is a full clock-less logic that controls the reset (\sym{CSA\_RST}) of the CSA upon the readout of a hit and drives the hit information through the column structure.  The internal structure of PL and its truth table are shown in figure~\ref{fig:PL} and table~\ref{tab:TT}.

\begin{minipage}[]{0.4\textwidth}
\centering
%\vspace{0pt}
\includegraphics[width=0.9\textwidth]{fig/PriorityLogicSCH.pdf}
\captionof{figure}{Schematic of in-pixel Priority Logic (PL) circuitry.}
\label{fig:PL}
\end{minipage}
%\hfill    % distance setting
\quad      % distance setting
\renewcommand{\arraystretch}{2}
\begin{minipage}[]{0.4\textwidth}
\tiny
\centering
%\vspace{-60pt}
\captionof{table}{Truth table of PL.}
\begin{tabular}{| c c c | c | c | c c |}
    \hline
    \multicolumn{3}{|c|}{Input} & \multicolumn{2}{c|}{\sym{AddrEN}} & \multicolumn{2}{c|}{Output}\\
    \hline
    \sym{PFI} & \sym{Hit} & \sym{COL\_RST} & Initial & Final & \sym{PFO} & \sym{CSA\_RST}\\
    \hline
    0 & 1 & 1 & 0 &                   0 & 0 & 0      \\
    0 & 1 & 1 & 1 &                   1 & 1 & 1      \\
    0 & 1 & 0 & X &                   1 & 1 & 0      \\
    0 & 0 & X & X &                   0 & 0 & 0      \\
    1 & X & X & X &                   0 & 1 & 0      \\
    \hline
  \end{tabular}
X$=$do not care.
\label{tab:TT}
\end{minipage}
\renewcommand{\arraystretch}{1}


\subsubsection{Column-wise priority chain and Address Bus (AB)}

The priority logic signals propagate in columns.  For the $i$th pixel, its \sym{PFI}$_i$ is
connected to the previous ($(i-1)$th) pixel's \sym{PFO}$_{i-1}$, and its \sym{PFO}$_i$ is fed
into the next ($(i+1)$th) pixel's \sym{PFI}$_{i+1}$.  Pixels in the same column are daisy-chained
in this fashion.  Every pixel of a given column has a unique hard-coded 7-bit address in the
form of pull-down switches.  Encoded pull-down switches are connected to the column-shared
Address Bus (AB) (green dashed box in figure~\ref{fig:pixStr}).  AB is weakly pulled up to all high
by default.  When \sym{AddrEN} becomes active in a pixel, it pulls down the AB to its own
unique address.  The topmost pixel (0th) in a column has \sym{PFI}$_0=0$.  If there is no hit in
any pixel (\sym{Hit} $=0$), the \sym{PFO} output is forced to 0 by \sym{G2}, \sym{G5} \&
\sym{G7}, which dictates that every pixel in the column has $\sym{PFI}=\sym{PFO}=0$.  When there
is no active \sym{COL\_RST} sent from the CRU module to every pixel in the column simultaneously,
the outputs of \sym{G3} \& \sym{G4} are forced to 0.  Once a pixel (e.g.\ $i$th) gets a hit, due
to the effects of \sym{G2}, \sym{G5} \& \sym{G7}, $\sym{PFO}_i=1$.  Forced by \sym{G7}, all
pixels below the $i$th pixel (denoted by $j$th, $j>i$) will have $\sym{PFI}_j=\sym{PFO}_j=1$.
Forced by \sym{G8}, any pixel with $\sym{PFI}=1$ won't enable \sym{AddrEN} even if it gets a hit.
The above described logic forms a column-wise priority chain: only the pixel with a hit that has
the lowest $i$ (highest priority) enables its \sym{AddrEN}, and it disables all the pixels lower
in the chain from asserting their individual addresses on the AB.

\subsubsection{Column Readout Unit (CRU)}

Each priority chain (column) is terminated by a Column Readout Unit (CRU) at the bottom of the
column.  CRU monitors the AB and validates the address change, then records the
7-bit address \& 10-bit time stamp for the corresponding hit pixel.  Upon the read of a hit, the
CRU asserts $\sym{COL\_RST}=1$, which is fed back simultaneously to all the pixels in the column.
Only the pixel that is pulling on the bus will respond to \sym{COL\_RST} (see figure~\ref{fig:PL}),
which results in the analog reset of the CSA ($\sym{CSA\_RST}=1$), the removal of hit, and the
release of the bus.  When the bus is successfully released, the address seen by the CRU returns
to all high.  The CRU senses such condition and outputs $\sym{Ready}=1$.  It indicates that a hit
has been registered in the column and has not yet been read by the MUX.  \sym{COL\_RST} and
\sym{Ready} are kept high until this CRU is read by the MUX.  \sym{R\_en} is set to high by the
MUX when it reads the associated CRU.

\subsubsection{Multiplexer (MUX)}

As shown in the blue dashed box of figure~\ref{fig:pixStr}, a digital multiplexer (MUX) polls the
status of each CRU sequentially, advancing at the falling edge of each clock cycle.  It picks up
the valid addresses and time stamps for the hit pixels from each CRU, then ships them off the
sensor.  A \sym{MARKER} signal is asserted when the 0th column is polled to indicate the start of
a frame.  The index of the column being read can be calculated externally referencing to
\sym{MARKER}.  A \sym{VALID} signal is asserted when the column being read has a hit.  The
address and time stamp outputs are valid only when $\sym{VALID}=1$.  

\subsection{Readout operation and timing}

A timing diagram of the readout process of a valid hit is shown in figure~\ref{fig:PriLogicSeq}.
It is assumed that there is only one hit pixel at Row 50, Column 0 and the system counter has
an initial value of $\sym{Sys\_Time[9:0]}=100$.  We also set \sym{Mask}$=1$ to enable the pixel response to hits.

\begin{figure}[htbp]
\centering % \begin{center}/\end{center} takes some additional vertical space
\includegraphics[width=.6\textwidth]{fig/TopmetalII-PriLogicSeq}
% "\includegraphics" from the "graphicx" permits to crop (trim+clip)
% and rotate (angle) and image (and much more)
\caption{Timing diagram of relevant signal activities during a hit and its readout.  In-pixel,
    in-column, in-CRU and in-MUX signals are indicated in red, green, blue and purple dashed
    boxes, respectively.}
\label{fig:PriLogicSeq}
\end{figure}

Charges arrive at $t_1$, causing the CSA output to exceed the threshold of the comparator,
resulting in $\sym{Flag}=1$.  Since $\sym{Mask}=1$, a hit is generated ($\sym{Hit}=1$); hence,
the single-pole-double-throw (\sym{SPDT}) switch (figure.~\ref{fig:pixStr}) grounds the gate of Mf
from its original bias \sym{FB\_VREF} so the CSA maximally retains the charge signal.  As
$\sym{PFI}=0$, \sym{PFO} and \sym{AddrEN} become 1 accordingly.  At this moment ($t_1$), the AB is pulled to the address of this pixel as well $\sym{Addr\_Bus}=50$.  At $t_2$ (rising edge of the clock in the CRU), the CRU senses the address change and outputs $\sym{Addr[6:0]}=50$, and waits for 4 clock cycles to confirm that the address change is not a transient phenomenon.  At $t_3$, the CRU latches the address value and the time stamp from the system counter $\sym{Time[9:0]}=\sym{Sys\_Time[9:0]}=105$.  It also sends a reset signal \sym{COL\_RST}$=1$ back to the column.  Although \sym{COL\_RST} is sent to every pixel in the column, forced by \sym{G3} in figure~\ref{fig:PL} , only the pixel that is pulling the AB and is being read out will respond to the reset.  The reset sets $\sym{CSA\_RST}=1$, which turns on the feedback transistor Mf, discharging $C_f$ so that the CSA output comes down towards the baseline.  At $t_4$, the CSA output falls below the threshold, causing $\sym{Hit}=0$ hence $\sym{AddrEN}=0$ and $\sym{PFO}=0$.  Once $\sym{AddrEN}=0$, CSA reset is done ($\sym{CSA\_RST}=0$) and \sym{Addr\_Bus} returns to all high.  The CRU also sets $\sym{Ready}=1$ indicating there is a valid hit waiting to be read.  Both the \sym{COL\_RST} and \sym{Ready} are removed when the CRU is polled at $t_7$.  The time between $t_4$ and $t_7$ is non-deterministic and can be as high as 72 clock cycles.  During $t_5\sim t_7$, the MUX is Polling the CRU and shipping the data $\sym{ADDR[6:0]}=50$ and $\sym{TIME[9:0]}=105$ off the sensor.  Since this pixel is in the 0th column, besides generating a $\sym{VALID}=1$, a synchronous \sym{MARKER} is also simultaneously asserted.  As the signal \sym{Polling} (\sym{R\_en}) is driven by the falling edge of the clock, it has a half-clock-cycle delay behind the \sym{MARKER}; therefore, it's high from $t_6$ to $t_8$.  When multiple pixels in the same column are hit simultaneously, the logic reads out and resets the hit pixels sequentially following their priorities in descending order.  No hit is missed.  

\section{Measurements and experimental results}\label{sec:mer}

\subsection{Threshold and noise}

We applied a repetitive tail pulse with an amplitude $\Vtp=10~{mV}$ on \sym{Gring} (see the top-left inset
in figure~\ref{fig:pixStr}).  An equivalent negative charge $Q_i=\Cinj\times\Vtp$ is injected at
every falling edge of the pulse into the CSA in every pixel simultaneously.  The response
amplitude of the CSA is expected to be $\Vtp\cdot(\Cinj/C_f)\approx16.5~{mV}$.  The CSA responds to both positive and
negative charges equally well; however, only the negative equivalent charge can bring the
\sym{CSA\_OUT} above the threshold to generate hits.  Also, we would like to avoid undershoots of
the CSA output due to positive charge injections; therefore, tail pulses are chosen over a square
wave.  The repetition rate of tail pulses is chosen to be low enough such that all the hit pixels
have sufficient time to be readout and reset before the next pulse arrives.

\begin{figure}[htbp]
\centering
\begin{minipage}[t]{.45\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{fig/s_curve}
  \captionof{figure}{A representative S-Curve of one pixel is shown.  The baseline median and width are determined with the $\text{probability}>2$ part of the curve (see text for details).  Inset shows the distribution of the width of the transition ($\sigma$) across all the pixels in the array.}
  \label{fig:S-Curve}
\end{minipage} 
\hfill
\begin{minipage}[t]{.45\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{fig/baseline_mid}
  \captionof{figure}{Baseline median distribution across the array in 2D (a, b) and 1D histogram (c) before
    (a and dashed line in c) and after (b and dotted line in c) trimming with a 4-bit DAC in every pixel.  Black points in (b) indicates the pixels are disabled.}
  \label{fig:baseline_mid}
\end{minipage} 
\end{figure}

As shown in figure~\ref{fig:S-Curve}, an S-Curve for a single pixel is obtained by scanning the
threshold, which is gradually lowered from well above the signal height where hit $\text{probability}=0$.  When the threshold is close to the injected signal height, a characteristic tapered transition from probability 0 to
1 due to noise appears.  When the threshold is close to the baseline, the logic registers a hit
every cycle regardless of the injected signal pulse; therefore, the computed probability is
bogusly well above 1.  When the threshold is well below the baseline, the logic saturates and
outputs no hit, although internally the discriminator constantly outputs 1.  We determine the
median and width of the baseline using the $\text{probability}>2$ part of the curve.  We fit the
transition part using the Cumulative Distribution Function (CDF) of Gaussian, 
$f(x)=\frac{1}{2}\left[1-\erf\left(\frac{x-\mu}{\sigma\sqrt{2}}\right)\right]$, to determine the mean ($\mu$) and width ($\sigma$) of the transition.  This procedure is repeated for every pixel in the array.  4-bit DACs are set to 0
for all pixels while the global \Vthg is varied to achieve the threshold scan.  Through the threshold
scan procedure for the entire array, we extracted the baseline and transition's location and
width from recorded S-Curves of every pixel.  The width ($\sigma$) of transition, which is an
indicator of the noise of CSA output presented to the comparator, has a mean value of
1.2~{mV} (see the inset in figure~\ref{fig:S-Curve}).  It is consistent with the analog noise
measurement reported in \cite{f}.  The distribution of the baseline median over the array is shown in figure~\ref{fig:baseline_mid}   (a) and (c). 

We write a set of values into the SRAM in each pixel to drive the 4-bit DAC to trim (reduce) the threshold differences between pixels in the array.  The set of DAC values, $\{n_i\}$, are calculated from the parameters extracted from the threshold scans.  All the 4-bit DACs share a globally adjustable step size \Vs.  $\Vthp{_i}=\Vs\times n_i$.  Ideally, \Vth should be as close to the baseline median while kept above the baseline noise width, to detect minimal signal amplitudes.  This requirement points to a small \Vs.  However, at the same time, \Vthp should cover a maximal threshold dispersion of the array in order to reduce the number of dysfunctional pixels due to insufficient trimming.  Since $n_i$ has only 16 values, it points to a large \Vs, contradicting the low threshold requirement.  To find a balanced set of parameters, we minimize the quadratic sum, $\sum\limits_i(\Vth-\text{baseline median})_i^2$, by varying $\{n_i\}$, \Vs and \Vthg.  We allow a small fraction of pixels with baselines that are far off to be excluded and subsequently disabled.  We also disable defective and noisy pixels by setting $\sym{Mask}=0$.  Disabled pixels are marked with black points in the 2D-figures.  A representative set of parameters are $\Vs=9~{mV}$, $\Vthg=532~{mV}$, and $10~{\%}$ disabled pixels.  After trimming with the optimized setting, we varied \Vthg to perform the threshold scan again.  The results show a greatly reduced width in baseline median distribution (figure~\ref{fig:baseline_mid}). 


\subsection{Imaging with pulsed LED illumination}

We placed a purple light LED $\sim2~{cm}$ above the top surface of a \TMIIm sensor.  The sensor is covered by an opaque photo mask with a transparent T-shaped pattern.  The T-shaped pattern is aligned with the center of the sensor (figure~\ref{fig:setup}).  The LED is driven by a train of narrow pulses with $10~{\mu}s$ width, $50~{ms}$ interval and 14~{V} Peak-to-Peak amplitude.  A $\sim1~{MHz}$ clock drives the CRUs and the MUX; therefore, the time it takes to read one frame (all 72 columns for once) is $T_f\approx72\times1~{\mu}s=72~{\mu}s$.  The sensor operates at the optimized threshold settings.  We recorded many frames of hits induced by a large number of LED pulses.  The photo mask was also rotated and displaced to cover different regions of the sensor.  Hit location and time are reconstructed from data.  A set of images showing the T-shape at four different orientations is
in figure~\ref{fig:T_four}.

\begin{figure}[htbp]
\centering
\begin{minipage}[t]{.45\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{fig/platform_diagram}
  \captionof{figure}{Light pulse injection setup.  Light from the LED is filtered by a photo mask with a T-shaped transparent opening before arriving at the sensor.}  
  \label{fig:setup}
\end{minipage} 
\hfill
\begin{minipage}[t]{.45\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{fig/T_four}
  \captionof{figure}{A set of images collected with the T-shaped photo mask placed at four different
    orientations.  Black points mark the disabled pixels.}
  \label{fig:T_four}
\end{minipage} 
\end{figure}



When a LED light pulse arrives at the sensor, multiple active pixels that receive the light
generate a \sym{Hit} in each of them.  Due to the column readout logic, hit pixels that are in
the same column will have only one pixel that has the highest priority registered by the CRU.
Since CRUs from each column work concurrently while reading off a single globally shared time,
each CRU registers the hit time of the highest priority pixel in its column.  Since the light
pulse arrives at each pixel simultaneously, the initially registered time, which is from the
highest priority pixel, is the same for all the CRUs (figure~\ref{fig:TimeInfo} (a)).  The MUX
reads the registered time from each CRU in a round-robin fashion from one column to the next.
When a CRU is read, the pixel of the highest priority in its column is reset, and the CRU
subsequently registers the second-highest priority pixel.  Since only the CRU has access to the
global time, the hit time of the second-highest as well as all the lower priority pixels is
determined by the readout rather than the actual arrival of the signal.  Only the hit time of the
highest priority pixel is physically meaningful.  It is worth noting that starting from the
second-highest priority pixel, the time difference between the $i$th-priority pixel and the
$(i+1)$th-priority pixel in the same column equals the number of columns (72), which is the time
interval between consecutive reads for a given CRU (readout time for one full frame).
figure~\ref{fig:TimeInfo} (b) illustrates this phenomenon.

\begin{figure}[htbp]
\centering % \begin{center}/\end{center} takes some additional vertical space
\includegraphics[width=.9\textwidth]{fig/light_spot_vector}
% "\includegraphics" from the "graphicx" permits to crop (trim+clip)
% and rotate (angle) and image (and much more)
\caption{Time stamping of hits.  (a) Time stamps printed at the location of corresponding pixels resulting from one light pulse.  The T-shaped light hit pattern is clearly visible.  Inset illustrates the readout of column No.~38 in this fashion.  (b) Time stamp read out as a function of clock cycle.  Frame $0$ reads out the initial time stamps from highest priority pixels in each column.  Subsequent frames read out pixels hit by the same light pulse but with progressively lower priority.}
\label{fig:TimeInfo}
\end{figure}

\section{Summary and outlook}\label{sec:sno}

We successfully implemented a CMOS pixel sensor, \TMIIm, for direct charge collection and imaging.  The detailed design, behavior and performance of a column-based priority logic readout in the sensor are presented.  The electrical measurements and imaging applications demonstrated the validity of such a readout scheme.  In the current design, although the in-array clock-less logic could drive the hit pixel's address to the edge of the array with minimal latency, the sequential logic nature of the CRU and the MUX limits the time it takes to discover the hit information to be beyond one clock cycle.  To further reduce the readout latency, analog and clock-less logic could be designed at the edge of the array to detect the activities in the AB promptly.  A polling style MUX could be replaced by a priority logic to read out the columns as well.  We will investigate these options in future \TM sensor development in addition to improving the array uniformity.

\section*{Acknowledgments}

This work is supported, in part, by the Thousand Talents Program at CCNU and by the National
Natural Science Foundation of China under Grant No.~11375073.  We also acknowledge the support
through the Laboratory Directed Research and Development funding from Berkeley Lab,
provided by the Director, Office of Science, of the U.S.\ Department of Energy under Contract
No.~DE-AC02-05CH11231.  We would like to thank Christine Hu-Guo and Nu Xu for fruitful
discussions.


% We suggest to always provide author, title and journal data:
% in short all the informations that clearly identify a document.

\begin{thebibliography}{99}

\bibitem{a}
J. Millaud and D. Nygren, \emph{The column architecture-a novel architecture for event driven 2D pixel imagers}, \emph{Nuclear Science Symposium and Medical Imaging Conference Record, 1995 IEEE} {\bf 1} (1995) 321 - 325.

\bibitem{b}
P. Yang et al., \emph{Low-power priority Address-Encoder and Reset-Decoder data-driven readout for Monolithic Active Pixel Sensors for tracker system}, \emph{Nucl. Instrum. Meth.} {\bf A 785} (2015) 61 - 69.

\bibitem{c}
M. Garcia-Sciveres et al., \emph{The FE-I4 pixel readout integrated circuit}, \emph{Nucl. Instrum. Meth.} {\bf A 636} (2011) S155 - S159.

\bibitem{d}
C. Hu-Guo et al., \emph{CMOS pixel sensor development: a fast read-out architecture with integrated zero suppression}, \emph{Journal of Instrumentation} {\bf 4} (2009) P04012.

\bibitem{e}
C. Hu-Guo et al., \emph{First reticule size MAPS with digital output and integrated zero suppression for the EUDET-JRA1 beam telescope}, \emph{Nucl. Instrum. Meth.} {\bf A 623} (2010) 480 - 482.

\bibitem{f}
M. An et al., \emph{A low-noise CMOS pixel direct charge sensor, \emph{\TMIIm}}, \emph{Nucl. Instrum. Meth.} {\bf A 810} (2016) 144 - 150.  [arXiv:1509.08611]


% Please avoid comments such as "For a review'', "For some examples",
% "and references therein" or move them in the text. In general,
% please leave only references in the bibliography and move all
% accessory text in footnotes.

% Also, please have only one work for each \bibitem.


\end{thebibliography}
\end{document}
